package cz.cvut.fit.ddw.brabeji.flight_classifier;

import cz.cvut.fit.ddw.brabeji.DatabaseRecordFactory;
import cz.cvut.fit.ddw.brabeji.LogisticModelParameters;
import org.apache.mahout.classifier.evaluation.Auc;
import org.apache.mahout.classifier.sgd.OnlineLogisticRegression;
import org.apache.mahout.math.Matrix;
import org.apache.mahout.math.RandomAccessSparseVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

public class FlightRunner {

    private String modelOutputFilename;
    private String tableName;

    public FlightRunner(String modelFilename, String dataTableName) {
        modelOutputFilename = modelFilename;
        tableName = dataTableName;
    }

    public void evaluate() throws IOException, SQLException {
        LogisticModelParameters lmp = LogisticModelParameters.loadFrom(new File(modelOutputFilename));
        DatabaseRecordFactory drf = new DatabaseRecordFactory(lmp.getTargetVariable(), lmp.getTypeMap(), lmp.useBias())
                .setMaxTargetValue(lmp.getMaxTargetCategories());

        OnlineLogisticRegression lr = lmp.createRegression();
        Auc collector = new Auc();

        ResultSet rs = drf.getRecordResultSet(this.tableName);
        while (rs.next()) {
            rs.getRow();
            Vector v = new SequentialAccessSparseVector(lmp.getNumFeatures());
            int actual = drf.processRow(rs, v);
            double score = lr.classifyScalar(v);
//            System.out.printf(Locale.ENGLISH, "%d,%.3f,%.6f%n", actual, score, lr.logLikelihood(actual, v));
            collector.add(actual, score);
        }
//        System.out.printf(Locale.ENGLISH, "AUC = %.2f%n", collector.auc());
        System.out.printf(Locale.ENGLISH, "<td>%.2f%n</td>", collector.auc());
//        Matrix m = collector.confusion();
//        System.out.printf(Locale.ENGLISH, "confusion: [[%.1f, %.1f], [%.1f, %.1f]]%n",
//                m.get(0, 0), m.get(1, 0), m.get(0, 1), m.get(1, 1));
//        m = collector.entropy();
//        System.out.printf(Locale.ENGLISH, "entropy: [[%.1f, %.1f], [%.1f, %.1f]]%n",
//                m.get(0, 0), m.get(1, 0), m.get(0, 1), m.get(1, 1));
    }

}
