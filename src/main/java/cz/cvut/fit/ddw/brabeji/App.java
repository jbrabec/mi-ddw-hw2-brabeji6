package cz.cvut.fit.ddw.brabeji;

import cz.cvut.fit.ddw.brabeji.flight_classifier.FlightRunner;
import cz.cvut.fit.ddw.brabeji.flight_classifier.FlightTrainer;

import java.util.Locale;

/**
 * Hello world!
 */
public class App {

    private static String modelFilename = "data/flights_model";

    public static void main(String[] args) throws Exception {

        for (int learningRate = 20; learningRate < 200; learningRate += 50) {
            for (double lambda = 5.0e-7; lambda > 1.0e-7; lambda -= 2.0e-7) {
                for (int numFeatures = 20; numFeatures < 200; numFeatures += 30) {

                    for (double decayExponent = 0.9; decayExponent > 0.7; decayExponent -= 0.1) {


                        System.out.printf(Locale.ENGLISH, "<tr>\n");
                        System.out.printf(Locale.ENGLISH, "<td>%d%n</td>\n", learningRate);
                        System.out.printf(Locale.ENGLISH, "<td>%6.1e</td>\n", lambda);
                        System.out.printf(Locale.ENGLISH, "<td>%d%n</td>\n", numFeatures);
                        System.out.printf(Locale.ENGLISH, "<td>%.2f%n</td>\n", decayExponent);

                        FlightTrainer ft = new FlightTrainer(modelFilename, "flights_transformed LIMIT 80000 OFFSET 0", 1, learningRate, lambda, numFeatures, decayExponent);
                        ft.trainModel();

                        FlightRunner fr = new FlightRunner(modelFilename, "flights_transformed LIMIT 1000000 OFFSET 80000");
                        fr.evaluate();
                        System.out.printf(Locale.ENGLISH, "</tr>\n");

                    }
                }
            }
        }

    }
}
