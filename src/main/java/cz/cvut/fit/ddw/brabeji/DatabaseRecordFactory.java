package cz.cvut.fit.ddw.brabeji;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.mahout.math.Vector;
import org.apache.mahout.vectorizer.encoders.ConstantValueEncoder;
import org.apache.mahout.vectorizer.encoders.ContinuousValueEncoder;
import org.apache.mahout.vectorizer.encoders.Dictionary;
import org.apache.mahout.vectorizer.encoders.FeatureVectorEncoder;
import org.apache.mahout.vectorizer.encoders.StaticWordValueEncoder;

public class DatabaseRecordFactory {
    private int maxTargetValue;
    private boolean includeBiasTerm;
    private String targetVariableName;
    private HashMap<String, Class<? extends FeatureVectorEncoder>> encoderClasses = new HashMap();
    private HashMap<String, FeatureVectorEncoder> encoders = new HashMap();
    private LinkedHashMap<String, String> typeMap;
    private final Map<String, Set<Integer>> traceDictionary = new TreeMap();
    private Dictionary targetDictionary = new Dictionary();

    public DatabaseRecordFactory(String targetName, LinkedHashMap<String, String> typeMap, boolean useBias) {
        this.includeBiasTerm = useBias;
        this.targetVariableName = targetName;
        this.encoderClasses.put("w", StaticWordValueEncoder.class);
        this.encoderClasses.put("n", ContinuousValueEncoder.class);
        this.typeMap = typeMap;
        Iterator bias = this.typeMap.keySet().iterator();

        while(bias.hasNext()) {
            String predictorName = (String)bias.next();

            try {
                Constructor e = ((Class)this.encoderClasses.get(this.typeMap.get(predictorName))).getConstructor(new Class[]{String.class});
                FeatureVectorEncoder encoder = (FeatureVectorEncoder)e.newInstance(new Object[]{predictorName});
                this.encoders.put(predictorName, encoder);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException var8) {
                var8.printStackTrace();
            }
        }

        if(this.includeBiasTerm) {
            ConstantValueEncoder bias1 = new ConstantValueEncoder("Intercept");
            this.encoders.put("Intercept", bias1);
            bias1.setTraceDictionary(this.traceDictionary);
        }

        this.targetDictionary.intern("on_time");
        this.targetDictionary.intern("delayed");
    }

    public int processRow(ResultSet row, Vector vector) throws SQLException {
        int targetValue = this.targetDictionary.intern(row.getString(this.targetVariableName));
        Iterator var5 = this.typeMap.keySet().iterator();

        while(var5.hasNext()) {
            String predictorVariableName = (String)var5.next();
            String value = row.getString(predictorVariableName);
            ((FeatureVectorEncoder)this.encoders.get(predictorVariableName)).addToVector(value, vector);
        }

        if(this.includeBiasTerm) {
            ((FeatureVectorEncoder)this.encoders.get("Intercept")).addToVector((String)null, 1.0D, vector);
        }

        return targetValue;
    }

    public DatabaseRecordFactory setMaxTargetValue(int max) {
        this.maxTargetValue = max;
        return this;
    }

    public DatabaseRecordFactory setIncludeBiasTerm(boolean b) {
        this.includeBiasTerm = b;
        return this;
    }

    public ResultSet getRecordResultSet(String tableName) {
        ResultSet rs = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:8889/flights", "root", "root");
            Statement e = conn.createStatement();
            String query = "SELECT * FROM " + tableName;
            rs = e.executeQuery(query);
        } catch (Exception var6) {
            var6.printStackTrace();
            System.exit(1);
        }

        return rs;
    }
}
