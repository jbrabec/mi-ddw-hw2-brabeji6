package cz.cvut.fit.ddw.brabeji.flight_classifier;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import cz.cvut.fit.ddw.brabeji.DatabaseRecordFactory;
import cz.cvut.fit.ddw.brabeji.LogisticModelParameters;
import org.apache.mahout.classifier.sgd.*;
import org.apache.mahout.clustering.Model;
import org.apache.mahout.math.RandomAccessSparseVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.vectorizer.encoders.ContinuousValueEncoder;
import org.apache.mahout.vectorizer.encoders.FeatureVectorEncoder;
import org.apache.mahout.vectorizer.encoders.StaticWordValueEncoder;

import java.io.*;
import java.sql.*;
import java.util.*;

public class FlightTrainer {

    private LogisticModelParameters lmp;
    private String modelOutputFilename;
    private String tableName;
    private int passes = 1;


    public FlightTrainer(String modelFilename, String dataTableName, int nPasses, int learningRate, double lambda, int numFeatures, double decayExponent) {
        modelOutputFilename = modelFilename;
        tableName = dataTableName;

        List<String> predictorList = new ArrayList<>();
//        predictorList.add("year");
//        predictorList.add("month");
//        predictorList.add("day");
//        predictorList.add("departure_time");
        predictorList.add("distance");
        predictorList.add("weekday");
        predictorList.add("at_night");
//        predictorList.add("delayed_departure");
        predictorList.add("origin");
        predictorList.add("destination");

        List<String> typeList = new ArrayList<>();
//        typeList.add("n");
//        typeList.add("n");
        typeList.add("n");
        typeList.add("w");

        lmp = new LogisticModelParameters();
        lmp.setTargetVariable("status");
        lmp.setMaxTargetCategories(2);
        lmp.setTypeMap(predictorList, typeList);
        lmp.setUseBias(false);
        lmp.setAlpha(1);
        lmp.setStepOffset(1000);

        passes = nPasses;
        lmp.setLearningRate(learningRate);
        lmp.setLambda(lambda);
        lmp.setNumFeatures(numFeatures);
        lmp.setDecayExponent(decayExponent);
//        passes = 1;
//        lmp.setLearningRate(50);
//        lmp.setLambda(5.0e-7);
//        lmp.setNumFeatures(20);
//        lmp.setDecayExponent(0.9);

    }


    public void trainModel() throws SQLException {

        OnlineLogisticRegression lr = lmp.createRegression();

        DatabaseRecordFactory drf = new DatabaseRecordFactory(lmp.getTargetVariable(), lmp.getTypeMap(), lmp.useBias())
                .setMaxTargetValue(lmp.getMaxTargetCategories());

        ResultSet rs = drf.getRecordResultSet(this.tableName);
        for (int pass = 0; pass < passes; pass++) {
            rs.first();
            while (rs.next()) {
                rs.getRow();
                RandomAccessSparseVector vector = new RandomAccessSparseVector(lmp.getNumFeatures());
                int actual = drf.processRow(rs, vector);
                lr.train(actual, vector);
            }
        }

        try {
            OutputStream modelOutput = new FileOutputStream(modelOutputFilename);
            lmp.saveTo(modelOutput);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
